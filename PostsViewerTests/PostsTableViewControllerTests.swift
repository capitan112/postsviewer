//
//  PostsViewerTests.swift
//  PostsViewerTests
//
//  Created by Oleksiy Chebotarov on 09/06/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

import XCTest
@testable import PostsViewer

class PostsTableViewControllerTests: XCTestCase {

    var sut: PostsTableViewController!
    var spy: PostsPresenterSpy!
    var window: UIWindow!
    
    override func setUp() {
        super.setUp()
        window = UIWindow()
        spy = PostsPresenterSpy()
        setupProductTableViewController()
    }

    override func tearDown() {
        window = nil
        spy = nil
    }
    
    // MARK: Test setup
    
    func setupProductTableViewController() {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        sut = storyboard.instantiateViewController(withIdentifier: "PostsTableViewController") as? PostsTableViewController
    }
    
    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
    
    class PostsPresenterSpy: PostsPresenterProtocol, PostsDataStore  {
        var viewController: PostsDisplayLogic?
        var networkService: NetworkServiceProtocol?
        var posts: [Post]?
        
        var loadData = false
        var routeToComments = false
        
        func loadingData() {
            loadData = true
        }
        
        func routeToCommentsViewController(source: UIViewController, post: Post) {
            routeToComments = true
        }
    }
    
    // MARK: Tests
    
    func testShouldDoLoadDataWhenCallLoadingData() {
        // Given
        let spy = PostsPresenterSpy()
        sut.postsPresenter = spy
        
        // When
        spy.loadingData()
        
        // Then
        XCTAssertTrue(spy.loadData, "loadingData() should ask the presenter to loadData")
    }

    func testShouldRouteToViewControllerWhenCallRouteToComments() {
        // Given
        let spy = PostsPresenterSpy()
        sut.postsPresenter = spy
        let post = Post(userId: 1, id: 1, title: "title", body: "body")
        // When
        spy.routeToCommentsViewController(source: UIViewController(), post: post)
        
        // Then
        XCTAssertTrue(spy.routeToComments, "routeToCommentsViewController() should ask the presenter to route")
    }
}
