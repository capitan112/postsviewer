//
//  Post.swift
//  PostsViewer
//
//  Created by Oleksiy Chebotarov on 09/06/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

import Foundation

struct Post: Codable {
    let userId : Int16
    let id : Int16
    let title : String
    let body : String
}
