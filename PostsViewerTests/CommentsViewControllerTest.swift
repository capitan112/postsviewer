//
//  CommentsViewControllerTest.swift
//  PostsViewerTests
//
//  Created by Oleksiy Chebotarov on 09/06/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

import XCTest
@testable import PostsViewer

class CommentsViewControllerTest: XCTestCase {

    var sut: CommentsViewController!
    var window: UIWindow!
    
    override func setUp() {
        super.setUp()
        window = UIWindow()
        setupProductTableViewController()
    }
    
    override func tearDown() {
        window = nil
    }
    
    // MARK: Test setup

    func setupProductTableViewController() {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        sut = storyboard.instantiateViewController(withIdentifier: "CommentsViewController") as? CommentsViewController
    }
    
    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
    
    class CommentsPresenterSpy: CommentsPresenterProtocol, CommentsDataStore  {
        var commentsViewController: DetailsDisplayProtocol?
        var post: Post?
        var user: User?
        var numberOfComments: Int?
        var loadData = false
        
        func loadingData() {
            loadData = true
        }
    }
    
    // MARK: Tests
    
    func testShouldDoLoadDataInCommetsVCWhenCallLoadingData() {
        // Given
        let spy = CommentsPresenterSpy()
        sut.commentsPresenter = spy

        // When
        spy.loadingData()
        
        // Then
        XCTAssertTrue(spy.loadData, "loadingData() should ask the presenter to loadData")
    }
}
