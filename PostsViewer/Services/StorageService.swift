//
//  StorageHandling.swift
//  PostsViewer
//
//  Created by Oleksiy Chebotarov on 11/06/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

import Foundation

protocol StorageServiceProtocol {
    static var shared: StorageService { get }
    
    func fetchPostsFromStorage() -> [Post]
}

class StorageService: StorageServiceProtocol {
    
    static let shared = StorageService()
    private init() {}
    
    public func savePosts(posts: [Post]) {
        StoredPosts.mr_truncateAll()
        
        for post in posts {
            var storedPosts: StoredPosts!
            
            if (storedPosts == nil) {
                storedPosts = StoredPosts.mr_createEntity()
            }
            
            storedPosts.id = post.id
            storedPosts.userId = post.userId
            storedPosts.body = post.body
            storedPosts.title = post.title
            storedPosts.managedObjectContext?.mr_saveToPersistentStoreAndWait()
        }
    }
    
    public func fetchPostsFromStorage() -> [Post] {
        let storedPosts = StoredPosts.mr_findAll() as? [StoredPosts]
        var posts = [Post]()
        if let storedPosts = storedPosts {
            for storedPost in storedPosts {
                let post = Post(userId: storedPost.userId, id: storedPost.id, title: storedPost.title!, body: storedPost.body!)
                posts.append(post)
            }
        }
        
        return posts
    }
    
    public func saveUsers(users: [User]) {
        StoredUsers.mr_truncateAll()

        for user in users {
            var storedUser: StoredUsers!

            if (storedUser == nil) {
                storedUser = StoredUsers.mr_createEntity()
            }

            storedUser.name = user.name
            storedUser.username = user.username
            storedUser.id = user.id
            storedUser.email = user.email
            storedUser.phone = user.phone
            storedUser.website = user.website
            storedUser.managedObjectContext?.mr_saveToPersistentStoreAndWait()
        }
    }
    
    public func fetchUserBy(userId: Int16) -> User? {
        let idPredicate = NSPredicate(format: "id = %@", argumentArray: [userId])
        let storedUser = StoredUsers.mr_findAll(with: idPredicate)?.first as? StoredUsers
        var user: User?
        if let storedUser = storedUser {
            user = User(storedUser: storedUser)
        }
        
        return user
    }
    
    public func saveComments(comments: [Comment]) {
        StoredComments.mr_truncateAll()

        for comment in comments {
            var storedComment: StoredComments!
            
            if (storedComment == nil) {
                storedComment = StoredComments.mr_createEntity()
            }
            
            storedComment.body = comment.body
            storedComment.email = comment.email
            storedComment.id = comment.id
            storedComment.name = comment.name
            storedComment.postId = comment.postId

            storedComment.managedObjectContext?.mr_saveToPersistentStoreAndWait()
        }
    }
    
    public func fetchCommentsNumberByPost(postId: Int16) -> Int {
        let idPredicate = NSPredicate(format: "postId = %@", argumentArray: [postId])
        let storedComments = StoredComments.mr_findAll(with: idPredicate)

        return storedComments?.count ?? 0
    }
}
