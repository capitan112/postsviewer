//
//  GCDMainThread.swift
//  PostsViewer
//
//  Created by Oleksiy Chebotarov on 09/06/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

import Foundation

func performUIUpdatesOnMain(updates: @escaping () -> Void) {
    DispatchQueue.main.async {
        updates()
    }
}
