//
//  Comments.swift
//  PostsViewer
//
//  Created by Oleksiy Chebotarov on 11/06/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

import Foundation

struct Comment: Codable {
    var postId: Int16
    var id: Int16
    var name: String
    var email: String
    var body: String
}
