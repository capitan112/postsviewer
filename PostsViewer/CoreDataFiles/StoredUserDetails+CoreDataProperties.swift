//
//  StoredUserDetails+CoreDataProperties.swift
//  
//
//  Created by Oleksiy Chebotarov on 12/06/2019.
//
//

import Foundation
import CoreData


extension StoredUserDetails {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StoredUserDetails> {
        return NSFetchRequest<StoredUserDetails>(entityName: "StoredUser")
    }

    @NSManaged public var email: String?
    @NSManaged public var id: Int16
    @NSManaged public var name: String?
    @NSManaged public var phone: String?
    @NSManaged public var username: String?
    @NSManaged public var website: String?
    @NSManaged public var users: NSSet?

}

// MARK: Generated accessors for users
extension StoredUserDetails {

    @objc(addUsersObject:)
    @NSManaged public func addToUsers(_ value: StoredPosts)

    @objc(removeUsersObject:)
    @NSManaged public func removeFromUsers(_ value: StoredPosts)

    @objc(addUsers:)
    @NSManaged public func addToUsers(_ values: NSSet)

    @objc(removeUsers:)
    @NSManaged public func removeFromUsers(_ values: NSSet)

}
