//
//  StoredPosts+CoreDataProperties.swift
//  
//
//  Created by Oleksiy Chebotarov on 12/06/2019.
//
//

import Foundation
import CoreData


extension StoredPosts {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StoredPosts> {
        return NSFetchRequest<StoredPosts>(entityName: "StoredPosts")
    }

    @NSManaged public var body: String?
    @NSManaged public var id: Int16
    @NSManaged public var title: String?
    @NSManaged public var userId: Int16
    @NSManaged public var comments: NSSet?
    @NSManaged public var posts: StoredUserDetails?

}

// MARK: Generated accessors for comments
extension StoredPosts {

    @objc(addCommentsObject:)
    @NSManaged public func addToComments(_ value: StoredComment)

    @objc(removeCommentsObject:)
    @NSManaged public func removeFromComments(_ value: StoredComment)

    @objc(addComments:)
    @NSManaged public func addToComments(_ values: NSSet)

    @objc(removeComments:)
    @NSManaged public func removeFromComments(_ values: NSSet)

}
