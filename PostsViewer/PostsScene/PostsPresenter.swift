//
//  PostsPresenter.swift
//  PostsViewer
//
//  Created by Oleksiy Chebotarov on 09/06/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

import Foundation
import UIKit

var postsUrl = "http://jsonplaceholder.typicode.com/posts"
var usersUrl = "http://jsonplaceholder.typicode.com/users"
var commemtsUrl = "http://jsonplaceholder.typicode.com/comments"

protocol PostsPresenterProtocol {
    func routeToCommentsViewController(source: UIViewController, post: Post)
    func loadingData()

    var viewController: PostsDisplayLogic? { set get }
    var networkService: NetworkServiceProtocol? { get set }
}

protocol PostsDataStore {
    var posts: [Post]? { get set}
}

class PostsPresenter: PostsPresenterProtocol, PostsDataStore {
    
    weak var viewController: PostsDisplayLogic?
    var networkService: NetworkServiceProtocol?
    var posts: [Post]?
    
    func loadingData() {
        let isConnected = Reachability.isConnectedToNetwork()
        if isConnected {
            networkService = NetworkService()
            getPostsFromWeb()
            getUsersFromWeb()
            getCommentsFromWeb()
        } else {
            self.posts = StorageService.shared.fetchPostsFromStorage()
            self.viewController?.reloadTableView()
        }
    }
    
    fileprivate func getUsersFromWeb() {
        networkService?.getUserDetailsBy(url: usersUrl, completion: {[unowned self] users, error in
            if let users = users {
                StorageService.shared.saveUsers(users: users)
            }
        })
    }
    
    fileprivate func getPostsFromWeb() {
        networkService?.getPostsBy(url: postsUrl, completion: {[unowned self] posts, error in
            if let posts = posts {
                StorageService.shared.savePosts(posts: posts)
            }
            self.posts = posts
            self.viewController?.reloadTableView()
        })
    }
    
    fileprivate func getCommentsFromWeb() {
        networkService?.getUserCommentsBy(url: commemtsUrl, completion: {[unowned self] comments, error in
            if let comments = comments {
                StorageService.shared.saveComments(comments: comments)
            }
        })
    }

    func routeToCommentsViewController(source: UIViewController, post: Post) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destinationVC = storyboard.instantiateViewController(withIdentifier: "CommentsViewController") as! CommentsViewController
        destinationVC.commentsPresenter?.post = post
        navigateToDetailVC(source: source, destination: destinationVC)
    }
    
    fileprivate func navigateToDetailVC(source: UIViewController, destination: CommentsViewController) {
        source.show(destination, sender: nil)
    }
}
