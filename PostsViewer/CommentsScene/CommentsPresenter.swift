//
//  DetailsPresenter.swift
//  PostsViewer
//
//  Created by Oleksiy Chebotarov on 10/06/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

import Foundation
import UIKit

protocol CommentsPresenterProtocol {
    var commentsViewController: DetailsDisplayProtocol? { get set }
    func loadingData()
}

protocol CommentsDataStore {
    var post: Post? { get set }
    var user: User? { get set }
    var numberOfComments: Int? { get set }
}

class CommentsPresenter: CommentsPresenterProtocol, CommentsDataStore {
    var commentsViewController: DetailsDisplayProtocol?
    
    var networkService: NetworkServiceProtocol?
    var post: Post?
    var user: User?
    var numberOfComments: Int?

    func loadingData() {
        if let userId = post?.userId, let postId = post?.id  {
            user = StorageService.shared.fetchUserBy(userId: userId)
            numberOfComments = StorageService.shared.fetchCommentsNumberByPost(postId: postId)
            commentsViewController?.updatePosts()
        }
    }
}
