//
//  StoredComment+CoreDataProperties.swift
//  
//
//  Created by Oleksiy Chebotarov on 12/06/2019.
//
//

import Foundation
import CoreData


extension StoredComment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StoredComment> {
        return NSFetchRequest<StoredComment>(entityName: "StoredComment")
    }

    @NSManaged public var body: String?
    @NSManaged public var email: String?
    @NSManaged public var id: Int32
    @NSManaged public var name: String?
    @NSManaged public var postId: Int16
    @NSManaged public var comments: StoredPosts?

}
