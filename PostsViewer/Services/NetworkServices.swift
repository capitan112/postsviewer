//
//  NetworkServices.swift
//  PostsViewer
//
//  Created by Oleksiy Chebotarov on 09/06/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

import UIKit

protocol NetworkServiceProtocol {
    func getPostsBy(url: String, completion: @escaping (_ searchedMovies: [Post]?, _ error: Error?) -> Void)
    func getUserDetailsBy(url: String, completion: @escaping (_ searchedDetails: [User]?, _ error: Error?) -> Void)
    func getUserCommentsBy(url: String, completion: @escaping (_ searchedComments: [Comment]?, _ error: Error?) -> Void)
}

class NetworkService: NSObject, NetworkServiceProtocol {

    fileprivate let session = URLSession.shared    
    func getUserCommentsBy(url: String, completion: @escaping (_ searchedComments: [Comment]?, _ error: Error?) -> Void) {
        self.getDataFrom(url: url, completion: { data, error in
            
            guard let data = data else {
                print("searched Comments error!")
                completion(nil, error)
                return
            }
            
            do {
                let comments = try JSONDecoder().decode([Comment].self, from: data)
                completion(comments, nil)
                
            } catch {
                print("JSON error: \(error.localizedDescription)")
                completion(nil, error)
            }
        })
    }
    
    func getUserDetailsBy(url: String, completion: @escaping (_ searchedDetails: [User]?, _ error: Error?) -> Void) {
        self.getDataFrom(url: url, completion: { data, error in

            guard let data = data else {
                print("searched UserDetail error!")
                completion(nil, error)
                return
            }
            
            do {
                let userDetails = try JSONDecoder().decode([User].self, from: data)
                completion(userDetails, nil)

            } catch {
                print("JSON error: \(error.localizedDescription)")
                completion(nil, error)
            }
        })
    }
    
    func getPostsBy(url: String, completion: @escaping (_ searchedPosts: [Post]?, _ error: Error?) -> Void) {
        self.getDataFrom(url: url, completion: { data, error in
            
            guard let data = data else {
                print("searched Posts error!")
                completion(nil, error)
                return
            }
            
            do {
                let posts = try JSONDecoder().decode([Post].self, from: data)
                completion(posts, nil)
                
            } catch {
                print("JSON error: \(error.localizedDescription)")
                completion(nil, error)
            }
        })
    }
    
    fileprivate func getDataFrom(url: String, completion: @escaping (_ results: Data?, Error?) -> Void) {
        
        guard let url = URL(string: url) else {
            print("wrong  searchURL")
            return
        }
        
        let urlRequest = URLRequest(url: url)
        
        let task = session.dataTask(with: urlRequest, completionHandler: { (data: Data?,
            response: URLResponse?, error: Error?) in
            
            if error != nil || data == nil {
                print("Client error!")
                completion(nil, error)
                return
            }
            
            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                print("Server error!")
                completion(nil, error)
                return
            }
            
            completion (data, nil)
        })
        
        task.resume()
    }
}
