//
//  CommentsViewController.swift
//  PostsViewer
//
//  Created by Oleksiy Chebotarov on 10/06/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

import UIKit

protocol DetailsDisplayProtocol: class {
    var commentsPresenter: (CommentsPresenterProtocol & CommentsDataStore)? { get set }
    
    func updatePosts()
}

class CommentsViewController: UIViewController, DetailsDisplayProtocol {

    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var bodyTextView: UITextView!
    
    var commentsPresenter: (CommentsPresenterProtocol & CommentsDataStore)?
    
    // MARK: Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        let viewController = self
        commentsPresenter = CommentsPresenter()
        commentsPresenter?.commentsViewController = viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commentsPresenter?.loadingData()
    }
    
     // MARK: - logic fo custom back
    fileprivate func popToNavController(alert: UIAlertAction!) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    // MARK: - delegate methods
    func updatePosts() {
        if let userName = commentsPresenter?.user?.name {
            authorLabel.text = String(describing: userName)
        }
        
        if let numberOfPosts = commentsPresenter?.numberOfComments {
            commentsLabel.text = String(describing: numberOfPosts)
        }
        
        bodyTextView.text = commentsPresenter?.post?.body
        bodyTextView.setContentOffset(.zero, animated: true)
    }
}
