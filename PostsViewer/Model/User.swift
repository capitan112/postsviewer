//
//  UserDetail.swift
//  PostsViewer
//
//  Created by Oleksiy Chebotarov on 10/06/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

import Foundation

struct User: Codable {
    var id: Int16
    var name: String
    var username: String
    var email: String
    var phone: String
    var website: String
    
    init(storedUser: StoredUsers) {
        self.id = storedUser.id
        self.name = storedUser.name!
        self.username = storedUser.username!
        self.email = storedUser.email!
        self.phone = storedUser.phone!
        self.website = storedUser.website!
    }
}
